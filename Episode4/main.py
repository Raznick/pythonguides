string = "Are Unicorns single corn(s)?"

print(string[2:])
print(string[:6])
print(string[1:10:3])
print(string[-8])

string1 = "Dwarfs"
string2 = "Orcs"
string3 = string1 + " & " + string2
print(string3)

print("a" * 9)
print("abc" * 9)

print(string.replace("Unicorns", "Singlecorns"))
print(string.upper())
print(string.lower())

print(string.startswith("Are "))
print(string.startswith("Spongebob "))
print(string.endswith("corn(s)?"))
print(string.endswith("Cats"))

name = input("Enter your name: ")
age = int(input("Enter your age: "))
print("Hello! {0}, You are {1} years old.".format(name, age))
print("{name} is {age} years old. Nice!".format(name=name, age=age))