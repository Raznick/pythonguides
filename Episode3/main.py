string1 = input("Enter a string: ")
print("This is a hard-coded string")
print(string1, "is not a hard-coded string :D")
print("TODO:")
print("Feed my dog", "Visit my grandmother", "Watch the last episode of Game of Thrones", sep=", ")

print("This line will not end with a new line!", end=" OMG ")
print("How surprising is it?!")

num1 = int(input("Enter an integer: "))
float1 = float(input("Enter a float: "))
print("NUMBERS!", num1, float1)