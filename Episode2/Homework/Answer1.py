# Check whether a number is odd or even.

odd_num = 15
even_num = 22

print("Odd numbers will always have a remainder of 1 when dividing by 2, thus, <num> % 2 should be equal to 1, or 0 for even numbers.")
print("The odd number:")
print(15 % 2)
print("The even number:")
print(22 % 2)