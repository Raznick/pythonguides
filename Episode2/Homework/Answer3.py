# Extract the number of thousands and hundreds from the number 1337, use EXACTLY the same formula for 12345.

num1 = 1337
num2 = 12345

print("1337 information")
print("hundreds")
print(num1 // 100 % 10)

print("thousands")
print(num1 // 1000 % 10)

print("12345 information")
print("hundreds")
print(num2 // 100 % 10)

print("thousands")
print(num2 // 1000 % 10)