# Check whether you can divide 42 by a certain number (without a remainder).

everything = 42
number1 = 21
number2 = 10

print("Generally, you can divide one number by the other when the remainder of the division operation is 0.")
print("42 % 21")
print(everything % number1)
print("42 % 10")
print(everything % number2)