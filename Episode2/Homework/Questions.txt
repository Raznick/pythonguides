1. Check whether a number is odd or even.
2. Check whether you can divide 42 by a certain number (without a remainder).
3. Extract the number of thousands and hundreds from the number 1337, use EXACTLY the same formula for 12345.

PAY ATTENTION!
-------------
- 	In the "Answer" files I use the 'print' function which has not been covered yet.
	If you do not understand what the hell is going on, it's OK.