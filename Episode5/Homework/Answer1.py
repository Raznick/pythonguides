my_list = [4, 1, 7, 9, 2]
print("Original list:", my_list)

# 1st way.
sorted_list = sorted(my_list)
print("Sorted list (1st):", sorted_list)
print("Original list was kept unchanged:", my_list)

# 2nd way
my_list.sort()
print("Sorted list (2nd):", my_list)
print("^ list.sort() changes the source list!")
