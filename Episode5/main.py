my_list = [1, 2, 3]
my_other_list = ["Cats", "Like", "Milk"]
my_final_list = ["What", "is", "better", "?", 3, "point", 5, "or", 3.5]

my_tuple = (1, 2, 3)

print(my_list[2])
print(my_tuple[2])
print(my_final_list[1:5])
print(my_final_list[-3])
print(my_final_list[1:7:3])

print(my_list + my_other_list)
    
my_list.append(12)
print(my_list)
my_list.reverse()
print(my_list)

unsorted_list = [4, 2, 9, 1, 20, 11]
unsorted_list.sort()
print(unsorted_list)

my_list.clear()
print(my_list)
